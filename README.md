# gen_version

This project calculates the version of a git project. 

It supports recursive git submodules.

# Run

In order to use `gen_version`, run the following:

```python
import Path.To.gen_version as gen_version
project_path = # absolute path to your project's main directory. Best practice is to use str(pathlib.Path(__file__).parent.parent....absolute())
is_HEAD_detached, project_hash, submodules_hashes, git_diffs = gen_version.get_version(project_path)
```
